= Sweet And Sour Chicken Wings

----
Recipe By     : 
Serving Size  : 12    Preparation Time : 0:00
Categories    : 
  Amount  Measure       Ingredient -- Preparation Method
--------  ------------  --------------------------------
      24         whole  chicken wings
     1/2      teaspoon  salt
     1/2      teaspoon  freshly ground black pepper
     1/4      teaspoon  garlic powder
     1/2           cup  ketchup
     1/2           cup  vinegar
     1/2           cup  sugar
     1/2           cup  water

Preheat oven to 350 F. Season wings with salt, pepper and garlic powder. Place in a shallow baking dish and bake for 15 minutes. in a small saucepan, combine ketchup, vinegar, sugar an

// tag::excludeDownstream[]

Source:

Nutr. Assoc. : 0

// end::excludeDownstream[]

----